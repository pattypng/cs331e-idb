import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING", 'postgresql://nayan@localhost:5432/postgres')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
db = SQLAlchemy(app)


# Relationship between holidays and countries
holiday_country_association = db.Table(
    'holiday_country',
    db.Column(
       'holiday_id', 
       db.Integer, 
       db.ForeignKey('holiday.id'), 
       nullable=False
    ), 
    db.Column(
        'iso', 
        db.String, 
        db.ForeignKey('country.iso'), 
        nullable=False
    )
)


class Date(db.Model):
    __tablename__ = 'date'

    date = db.Column(db.Date, primary_key=True)
    day_of_week = db.Column(db.String, nullable=False)
    formatted = db.Column(db.String, nullable=False)


class Holiday(db.Model):
    __tablename__ = 'holiday'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    type = db.Column(db.String, nullable=False)
    description = db.Column(db.String)
    fixed = db.Column(db.Boolean, nullable=False)

    date = db.Column(
        db.Date, 
        db.ForeignKey('date.date')
    )
    countries = db.relationship(
        'Country', 
        secondary=holiday_country_association,
        cascade='all, delete', 
        backref=db.backref('holidays')
    )


class Country(db.Model):
    __tablename__ = 'country'

    iso = db.Column(db.String(2), primary_key=True)
    name = db.Column(db.String, nullable=False)
    capital = db.Column(db.String, nullable=False)
    population = db.Column(db.Integer, nullable=False)
    region = db.Column(db.String, nullable=False)


