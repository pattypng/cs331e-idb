
$(document).ready(function(){
    $('.interactive-table').each(function() {
        tableInteractivity($(this));
    });
});

function tableInteractivity($table) {
    var $rows = $table.children('table').children('tbody').children('tr');
    $rows.slice(0, 5).addClass('active');

    toggleButtonVisibility($table, $rows);
    
    $('a.show-less').on('click', function(e) {
        e.preventDefault();  
        var lastActiveIndex = $rows.filter('.active:last').index();
        $rows.slice(Math.max(5, lastActiveIndex - 4), lastActiveIndex + 1).removeClass('active');
        
        toggleButtonVisibility($table, $rows);
    });

    $('a.show-more').on('click', function(e) {
        e.preventDefault();  
        var lastActiveIndex = $rows.filter('.active:last').index();
        $rows.slice(lastActiveIndex + 1, lastActiveIndex + 6).addClass('active');
        
        toggleButtonVisibility($table, $rows);
    });

    $('a.show-all').on('click', function(e) {
        e.preventDefault();  
        $rows.filter(':hidden').addClass('active');
        
        toggleButtonVisibility($table, $rows);
    });
}

function toggleButtonVisibility($table, $rows) {
    $table.find('a.show-less').toggle($rows.filter('.active').length > 5);
    $table.find('a.show-more').toggle($rows.filter(':hidden').length !== 0);
    $table.find('a.show-all').toggle($rows.filter(':hidden').length !== 0);
}

function underlineSearch($results, searchQuery) {
    $names = $results.find('h1.name');
    $names.each(function() {
        underlineTerms($(this), searchQuery);
    });
}

function underlineTerms($name, searchQuery) {
    for (searchTerm of searchQuery.split(' ')) {
        html = $name.html();
        start = html.toLowerCase().search(searchTerm.toLowerCase());
        end = start + searchTerm.length;
        if (start === -1) {
            continue;
        }
        $name.html(html.slice(0, start) + '<u>' + html.slice(start, end) + '</u>' + html.slice(end));
    }
}
