from datetime import datetime
from flask import Flask, render_template, url_for, request, redirect, jsonify
from sqlalchemy import func, desc, cast, Text

from create_db import app, db, Date, Holiday, Country


app.config['JSON_AS_ASCII'] = False


def convert_date_to_string(date):
    '''Converts a datetime object into a string in the format 'YYYY-MM-DD'.'''
    return date.strftime('%Y-%m-%d')


def convert_string_to_date(date):
    '''Converts a string in the format 'YYYY-MM-DD' into a datetime object.'''
    return datetime.strptime(date, '%Y-%m-%d').date()


def find_primary_key(orm_object):
    '''Finds the name of the primary key of an ORM object.
    Returns a tuple containing the name of the primary key and a list of all the non-primary keys.
    '''
    columns = orm_object.__table__.columns
    primary_key = None
    non_primary_keys = []
    for column in columns:
        if column.primary_key:
            primary_key = column.name
        else:
            non_primary_keys.append(column.name)
    return primary_key, non_primary_keys
        

def make_dictionary(orm_objects):
    '''Converts a generic ORM object from an SQLAlchemy query into a dictionary of the form {
        'primary_key': {
            'column_name': column_value,
            ...
        },
        ...
    }.'''
    if not orm_objects:
        return {}
    
    orms_dictionary = {}
    primary_key, non_primary_keys = find_primary_key(orm_objects[0])
    for orm_object in orm_objects:
        orms_dictionary[getattr(orm_object, primary_key)] = {
            column_name: getattr(orm_object, column_name)
            for column_name in non_primary_keys
        }
    return orms_dictionary


def make_dates_dictionary(dates):
    '''Converts a Date object from an SQLAlchemy query into a dictionary of the form specified in make_dictionary(orm_object) where the date primary key is formatted as a string in the format 'YYYY-MM-DD'.'''
    dates_dictionary = make_dictionary(dates)
    new_dictionary = {}
    for key, value in dates_dictionary.items():
        new_key = convert_date_to_string(key)
        new_dictionary[new_key] = value
    return new_dictionary


def make_holidays_dictionary(holidays):
    '''Converts a Holiday object from an SQLAlchemy query into a dictionary of the form specified in make_dictionary(orm_object) where the date value is formatted as a string in the format 'YYYY-MM-DD'.'''
    holidays_dictionary = make_dictionary(holidays)
    new_dictionary = {}
    for key, value in holidays_dictionary.items():
        new_dictionary[key] = value
        new_dictionary[key]['date'] = \
            convert_date_to_string(value['date'])
    return new_dictionary


def make_countries_dictionary(countries):
    '''Converts a Country object from an SQLAlchemy query into a dictionary of the form specified in make_dictionary(orm_object).'''
    return make_dictionary(countries)


def make_page_info(pagination):
    '''Converts a Pagination object from an SQLAlchemy query into a dictionary of the form {
        'page': int,
        'first_item': int,
        'last_item': int,
        'total_pages': int,
        'total_items': int
    }.'''
    return {
        'page': pagination.page,
        'first_item': pagination.per_page * (pagination.page - 1)+ 1,
        'last_item': min(pagination.per_page * pagination.page, pagination.total),
        'total_pages': pagination.pages,
        'total_items': pagination.total
    }


@app.route('/api/dates', methods=['GET'])
def get_dates():
    '''Returns a JSON object containing the dates in the database. 

    Query Parameters: 
        specify: comma-separated list of dates in the format 'YYYY-MM-DD' -> only returns dates that match the specified dates
        active: no value -> only dates that have holidays are returned
        page: int -> returns dates that belong on the specified page along with separate key-value pair for pagination information

    JSON Format: {
        'dates': {
            'YYYY-MM-DD': {
                'day_of_week': string,
                'formatted': string,
                'url': string
            },
            ...
        },
        'page_info' (optional): format of make_page_info(pagination)
    }
    '''
    outp = {}
    dates_query = Date.query
    specify = request.args.get('specify')
    if specify:
        specify_list = request.args.get('specify').split(',')
        specify_as_datetime = [
            datetime.strptime(date, '%Y-%m-%d').date()
            for date in specify_list
        ]
        dates_query = dates_query.filter(
            Date.date.in_(specify_as_datetime)
        )

    if 'active' in request.args:
        dates_query = dates_query.filter(
            Holiday.query.filter(
                Date.date == Holiday.date
            ).exists()
        )
    
    if 'page' in request.args:
        page = int(request.args.get('page'))
        dates_query = dates_query.order_by(Date.date)
        pagination = dates_query.paginate(page=page, per_page=20)
        dates = pagination.items
        outp['page_info'] = make_page_info(pagination)
    else:
        dates = dates_query.all()

    outp['dates'] = make_dates_dictionary(dates)
    return jsonify(outp)


@app.route('/api/dates/<date>', methods=['GET'])
def get_date(date):
    '''Returns a JSON object containing the specified date of format 'YYYY-MM-DD'.
    
    JSON Format: {
        'YYYY-MM-DD': {
            'day_of_week': string,
            'formatted': string,
            'url': string
        }
    }
    '''
    date = Date.query.get(
        datetime.strptime(date, '%Y-%m-%d').date()
    )
    return make_dates_dictionary([date])


@app.route('/api/holidays', methods=['GET'])
def get_holidays():
    '''Returns a JSON object containing the holidays in the database.

    Query Parameters:
        type: string of 'PUBLIC', 'RELIGIOUS', or 'OBSERVANCE' -> only returns holidays that match the specified type.
        country: string of a country code -> only returns holidays that are celebrated in the specified country.
        date: string of a date in the format 'YYYY-MM-DD' -> only returns holidays that occur on the specified date.
        specify: comma-separated list of holiday IDs -> only returns holidays that match the specified IDs.
        search: string -> only returns countries that match search value.
        sort: string, of form (alpha, date)-(asc, desc)-> adds a key-value pair named ordered_keys to the JSON object containing a list of holiday ids in the specified order. eg. name-desc returns holiday ids sorted alphabetically in reverse.
        page: int -> returns holidays that belong on the specified page along with separate key-value pair for pagination information.

    JSON Format: {
        'holidays': {
            'holiday_id': {
                'date': string,
                'description': string,
                'fixed': bool,
                'name': string,
                'type': string,
                'url': string
            },
            ...
        },
        'ordered_keys': list of holiday ids in order requested by sort query parameter,
        'page_info':  format of make_page_info(pagination)
    }    
    '''
    outp = {}
    holidays_query = Holiday.query
    type = request.args.get('type')
    if type:
        holidays_query = holidays_query.filter_by(type=type)

    country = request.args.get('country')
    if country:
        holidays_query = holidays_query.filter(
            Holiday.countries.any(iso=country)
        )

    date = request.args.get('date')
    if date:
        holidays_query = holidays_query.filter_by(
            date=datetime.strptime(date, '%Y-%m-%d').date()
        )

    specify = request.args.get('specify')
    if specify:
        specify_list = list(map(int, specify.split(',')))
        holidays_query = holidays_query.filter(
            Holiday.id.in_(specify_list)
        )

    search = request.args.get('search')
    if search:
        search_pattern = f"%{search}%"  # Create a pattern to search for substrings
        holidays_query = holidays_query.filter(
            (Holiday.name.ilike(search_pattern)))

    sort = request.args.get('sort', 'alpha-asc')
    if sort:
        param, direction = sort.split('-')
        if param == 'alpha':
            if direction == 'asc':
                holidays_query = holidays_query.order_by(Holiday.name)
            elif direction == 'desc':
                holidays_query = holidays_query.order_by(Holiday.name.desc())
            else:
                raise ValueError('Invalid sort direction')
        elif param == 'date':
            if direction == 'asc':
                holidays_query = holidays_query.order_by(Holiday.date)
            elif direction == 'desc':
                holidays_query = holidays_query.order_by(Holiday.date.desc())
            else:
                raise ValueError('Invalid sort direction')
        else:
            raise ValueError('Invalid sort parameter')

    page = request.args.get('page')
    if page:
        page = int(page)
        pagination = holidays_query.paginate(page=page, per_page=20)
        holidays = pagination.items
        outp['page_info'] = make_page_info(pagination)
    else:
        holidays = holidays_query.all()

    outp['ordered_keys'] = [
        holiday.id for holiday in holidays
    ]

    outp['holidays'] = make_holidays_dictionary(holidays)
    return jsonify(outp)


@app.route('/api/holidays/<id>', methods=['GET'])
def get_holiday(id):
    '''Returns a JSON object containing the specified holiday of ID.
    
    JSON Format: {
        'holiday_id': {
            'date': string,
            'description': string,
            'fixed': bool,
            'name': string,
            'type': string,
            'url': string
        }
    }
    '''
    holiday = Holiday.query.get(id)
    return make_holidays_dictionary([holiday])


@app.route('/api/countries', methods=['GET'])
def get_countries():
    '''Returns a JSON object containing the countries in the database.

    Query Parameters:
        specify: comma-separated list of country codes -> only returns countries that match the specified country codes.
        celebrate: int -> only returns countries that celebrate the specified holiday ID.
        search: string -> only returns countries that match search value.
        sort: string, of form (alpha, pop)-(asc, desc)-> adds a key-value pair named ordered_keys to the JSON object containing a list of country codes in the specified order. eg. pop-desc returns country codes sorted by descending population.
        page: int -> returns countries that belong on the specified page along with separate key-value pair for pagination information.

    JSON Format: {
        'countries': {
            'iso': {
                'name': string,
                'capital': string,
                'population': string,
                'region': string
            },
            ...
        },
        'ordered_keys': list of country codes in order requested by sort query parameter,
        'page_info' (optional):  format of make_page_info(pagination)
    }
    '''
    outp = {}
    countries_query = Country.query
    specify = request.args.get('specify')
    if specify:
        specify_list = specify.split(',')
        countries_query = countries_query.filter(
            Country.iso.in_(specify_list)
        )

    celebrate = request.args.get('celebrate')
    if celebrate:
        countries_query = countries_query.filter(
            Country.holidays.any(id=celebrate)
        )
    
    search = request.args.get('search')
    if search:
        search_pattern = f"%{search}%"  # Create a pattern to search for substrings
        countries_query = countries_query.filter(
            (Country.name.ilike(search_pattern)))

    sort = request.args.get('sort', 'alpha-asc')
    if sort:
        param, direction = sort.split('-')
        if param == 'alpha':
            if direction == 'asc':
                countries_query = countries_query.order_by(Country.name)
            elif direction == 'desc':
                countries_query = countries_query.order_by(Country.name.desc())
            else:
                raise ValueError('Invalid sort direction')
        elif param == 'pop':
            if direction == 'asc':
                countries_query = countries_query.order_by(Country.population)
            elif direction == 'desc':
                countries_query = countries_query.order_by(Country.population.desc())
            else:
                raise ValueError('Invalid sort direction')
        else:
            raise ValueError('Invalid sort parameter')
    
    page = request.args.get('page')
    if page:
        page = int(page)
        pagination = countries_query.paginate(page=page, per_page=20)
        countries = pagination.items
        outp['page_info'] = make_page_info(pagination)
    else:
        countries = countries_query.all()

    outp['ordered_keys'] = [
        country.iso for country in countries
    ]

    outp['countries'] = make_countries_dictionary(countries)
    return jsonify(outp)


@app.route('/api/countries/<iso>', methods=['GET'])
def get_country(iso):
    '''Returns a JSON object containing the country of the specified ISO.
    
    JSON Format: {
        'iso': {
            'name': string,
            'capital': string,
            'population': string,
            'region': string
        }
    }
    '''
    country = Country.query.get(iso)
    return make_countries_dictionary([country])


@app.route('/api/docs', methods=['GET'])
def docs():
    return redirect('https://documenter.getpostman.com/view/32256698/2sA3BgBvix')


def main():
    ...
    

if __name__ == "__main__":
    main()

