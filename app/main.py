from pydoc import pager
import requests
import json
from flask import Flask, render_template, url_for, request, redirect
from create_db import app, db, Date, Holiday, Country, create_everything
from api import *

# Creates the DB
# create_everything()

# This dictionary is hard-coded and is used for the purpose of date verification within the dates page
month_and_day = {1: 31, 2: 29, 3: 31, 4: 30, 5: 31, 6: 30, 
          7: 31, 8: 31, 9: 30, 10: 31, 11: 30, 12: 31}

# This dictionary is hard-coded and is used for the purpose of getting the month name based on the month the user has chosen on the dates page
month_names = {'01': 'January', '02': 'February', '03': 'March', '04': 'April', '05': 'May', '06': 'June', 
               '07': 'July', '08': 'August', '09': 'September', '10': 'October', '11': 'November', '12': 'December'}

# Website home page route
@app.route('/')
def index():
    return render_template('index.html')


# Website about page route
@app.route('/about')
def about():
    GITLAB_ACCESS_TOKEN = 'glpat-yS2qdgrBXPLTm7Q4_haS'
    COMMITS_ENDPOINT = 'https://gitlab.com/api/v4/projects/54208647/repository/commits'
    ISSUES_ENDPOINT = 'https://gitlab.com/api/v4/projects/54208647/issues'
    HEADERS = {'PRIVATE-TOKEN': GITLAB_ACCESS_TOKEN}
    
    commits = []
    commits_params = {
        'page': 1,
        'per_page': 100
    }
    while True:
        raw_commits_request = requests.get(
            COMMITS_ENDPOINT,
            headers=HEADERS,
            params=commits_params
        )
        if not len(raw_commits_request.json()):
            break
        commits.extend(raw_commits_request.json())
        commits_params['page'] += 1
        
    issues = []
    issues_params = {
        'page': 1,
        'per_page': 100
    }
    while True:
        raw_issues_request = requests.get(
            ISSUES_ENDPOINT,
            headers=HEADERS,
            params=issues_params
        )
        if not len(raw_issues_request.json()):
            break
        issues.extend(raw_issues_request.json())
        issues_params['page'] += 1
        
    commits_counts = {'Total': len(commits)}
    for commit in commits:
        name = commit['committer_name']
        commits_counts[name] = \
            commits_counts.get(name, 0) + 1
    if commits_counts['pattypng']:
        commits_counts['Patty Chow'] += commits_counts['pattypng']
        del commits_counts['pattypng']

    issues_counts = {'Total': len(issues)}
    for issue in issues:
        if issue['closed_by']:
            name = issue['closed_by']['name']
        issues_counts[name] = \
            issues_counts.get(name, 0) + 1
    print(commits_counts, issues_counts)
    return render_template('about.html', commits=commits_counts, issues=issues_counts)

# Since the dates search requires a list of relevant holidays, this function calls the API to get all holidays
def clean_holidays():
    holidays_request = requests.get(
        url_for('get_holidays', _external=True))
    raw_holidays = holidays_request.json()

    holidays_data = raw_holidays.get('holidays', {})

    # Creates a list of dictionaries containing the holiday_id, name, date, and type
    holidays_list = [{'holiday_id': holiday_id, **holiday_data} for holiday_id, holiday_data in holidays_data.items()]
    for holiday in holidays_list:
        holiday['type'] = (holiday['type'][0]) + (holiday['type'][1:].lower())

    sorted_holidays = sorted(holidays_list, key=lambda x: x['date'])
    return sorted_holidays

# Website date page route
@app.route('/dates', methods=['GET', 'POST'])
def dates_splash():
    # Gets the list of dictionaries mentioned previously
    holidays_list = clean_holidays()

    # If the user has submitted data (A month name or specific date), the search for holidays by date is conducted
    if request.method == 'POST':
        if 'months' in request.form:
            error_message = "There are currently no registered holidays for this month."
            month = request.form['months']

            for holiday in holidays_list:
                # Holidays dates are given the format 'YYYY-MM-DD', this indexes just for the month and given month code
                if month == holiday['date'][5:7]:
                    date_code = holiday['date'][5:7]
                    return redirect(url_for('dates_by_month', date_code=date_code))
            return render_template('dates/splash.html', error_message=error_message)    
        else:
            error_message = "To search, please enter a valid date!"
            date = request.form['date']
            # Ensures that the user-text provided is a valid date
            try:
                month = int(date[0:2])
                day = int(date[3:])
            except ValueError:
                return render_template('dates/splash.html', error_message=error_message)
            if (int(date[0:2]) in range(0, 13)):
                if int(date[3:]) <= month_and_day[int(date[0:2])]:
                    for holiday in holidays_list:
                        if (date[0:2] == holiday['date'][5:7]) and (date[3:] == holiday['date'][8:]):
                            date_code = holiday['date']
                            return redirect(url_for('dates_by_day', date_code=date_code))
                    error_message = "There are currently no registered holidays for this date."
                    return render_template('dates/splash.html', error_message=error_message)        
                else:
                    return render_template('dates/splash.html', error_message=error_message)
            else:
                return render_template('dates/splash.html', error_message=error_message)
    else:
        return render_template('dates/splash.html', error_message='')

# Website page route for showing holidays by specific day
@app.route('/dates_day/<date_code>', methods=['GET', 'POST'])
def dates_by_day(date_code):
    holiday_list = clean_holidays()
    holiday_by_day = []
    for holiday in holiday_list:
        if holiday['date'] == date_code:
            holiday_by_day.append(holiday)
    return render_template('dates/daily.html', holiday_by_day=holiday_by_day)

# Website page route for showing holidays by specific month
@app.route('/dates_month/<date_code>', methods=['GET', 'POST'])
def dates_by_month(date_code):
    holiday_list = clean_holidays()
    holidays_by_month = []
    month = month_names[date_code]
    for holiday in holiday_list:
        if holiday['date'][5:7] == date_code:
            holidays_by_month.append(holiday)

    return render_template('dates/monthly.html', holidays_by_month=holidays_by_month, month=month)

# Website page route for showing holidays
@app.route('/holidays', methods=['GET', 'POST'])
def holidays_splash():

    if request.method == 'POST':
        sort_query = request.form['sort']
        search_query = request.form['holidays']
        if not sort_query and not search_query:
            return redirect(url_for('holidays_splash'))
        if not sort_query:
            return redirect(url_for('holidays_splash', search=search_query))
        if not search_query:
            return redirect(url_for('holidays_splash', sort=sort_query))
        return redirect(url_for('holidays_splash', search=search_query, sort=sort_query))

    page = request.args.get('page', 1, type=int)
    search_query = request.args.get('search', None)
    sort = request.args.get('sort', 'alpha-asc')

    request_params = {
        'page': page
    }
    if search_query:
        request_params['search'] = search_query
    if sort:
        request_params['sort'] = sort
    # API call for all holidays
    holidays_request = requests.get(
        url_for('get_holidays', _external=True),
        params=request_params
    )
    # accesses the value of holidays which is the large dictionary of all the holidays
    raw_holidays = holidays_request.json()
    page_info  = raw_holidays['page_info']

    # creates a table of the names of each holiday in the dictionary
    holidays = [(holiday_id, raw_holidays['holidays'][str(holiday_id)]['name'], raw_holidays['holidays'][str(holiday_id)]['date']) 
                for holiday_id in raw_holidays['ordered_keys']]

    # renders holidays splash while passing the table of holiday names to the splash file
    return render_template('holidays/splash.html', holidays=holidays, page_info=page_info)

# Website page route for showing a specific holiday
@app.route('/holidays/<id>')
def holidays(id):
    # API call for holiday by ID
    holiday_request = requests.get(
        url_for('get_holiday', id=id, _external=True)
    )
    raw_holiday = holiday_request.json()

    holiday = {
        'name' : raw_holiday[id]['name'],
        'id': id,
        'description': raw_holiday[id]['description'],
        'date': raw_holiday[id]['date'],
        'type' : raw_holiday[id]['type']
    }

    countries_request = requests.get(
        url_for('get_countries', _external=True),
        params={
            'celebrate' : id
        }
    )

    raw_countries = countries_request.json()

    countries = [
        [(country['name'], url_for('countries', iso=iso))]
        for iso, country in raw_countries['countries'].items()
    ]

    return render_template('holidays/holiday.html', holiday = holiday, countries = countries)
    '''
    for holiday in holiday_and_date_data:
        if holiday['name'] == holiday_name:
            display_name = holiday_name.replace ('-', ' ')
            return render_template('holidays/holiday.html', holiday=holiday, display_name = display_name)
    '''
    # return render_template('holidays_splash.html')


def format_population(population):
    if population < 1e3:
        return f'{population}K'
    elif population < 1e6:
        return f'{population / 1e3:.1f}M'
    elif population < 1e9:
        return f'{population / 1e6:.1f}B'
    

# Website page route for showing all countries
@app.route('/countries', methods=['GET', 'POST'])
def countries_splash():
    if request.method == 'POST':
        sort_query = request.form['sort']
        search_query = request.form['countries']
        if not sort_query and not search_query:
            return redirect(url_for('countries_splash'))
        if not sort_query:
            return redirect(url_for('countries_splash', search=search_query))
        if not search_query:
            return redirect(url_for('countries_splash', sort=sort_query))
        return redirect(url_for('countries_splash', search=search_query, sort=sort_query))
    

    page = request.args.get('page', 1, type=int)
    search_query = request.args.get('search', None)
    sort = request.args.get('sort', 'alpha-asc')
    # API call for all countries
    request_params = {
        'page': page
    }
    if search_query:
        request_params['search'] = search_query
    if sort:
        request_params['sort'] = sort
    countries_request = requests.get(
        url_for('get_countries', _external=True),
        params=request_params
    )
    raw_response = countries_request.json()

    countries = [
        (iso, raw_response['countries'][iso]['name'])
        for iso in raw_response['ordered_keys']
    ]
    page_info = raw_response['page_info']

    return render_template(
        'countries/splash.html', 
        countries=countries,
        page_info=page_info
    )


# Website page route for showing countries that celebrate a specific holiday, given through holiday_id
@app.route('/countries/celebrate/<holiday_id>', methods=['GET', 'POST'])
def countries_by_holiday(holiday_id):
    if request.method == 'POST':
        sort_query = request.form['sort']
        search_query = request.form['countries']
        if not sort_query and not search_query:
            return redirect(url_for('countries_by_holiday', holiday_id=holiday_id))
        if not sort_query:
            return redirect(url_for('countries_by_holiday', holiday_id=holiday_id, search=search_query))
        if not search_query:
            return redirect(url_for('countries_by_holiday', holiday_id=holiday_id, sort=sort_query))
        return redirect(url_for('countries_by_holiday', holiday_id=holiday_id, search=search_query, sort=sort_query))

    page = request.args.get('page', 1, type=int)
    search_query = request.args.get('search', None)
    sort = request.args.get('sort', 'alpha-asc')
    request_params = {
        'page': page,
        'celebrate': holiday_id
    }
    if search_query:
        request_params['search'] = search_query
    if sort:
        request_params['sort'] = sort
    countries_request = requests.get(
        url_for('get_countries', _external=True),
        params=request_params
    )
    raw_response = countries_request.json()

    countries = [
        (iso, country['name'])
        for iso, country in raw_response['countries'].items()
    ]
    page_info = raw_response['page_info']

    return render_template(
        'countries/splash.html', 
        countries=countries,
        page_info=page_info
    )

# Website page route for showing a specific country
@app.route('/countries/<iso>')
def countries(iso):
    iso = iso.upper()
    # API call for country by ISO
    country_request = requests.get(
        url_for('get_country', iso=iso, _external=True)
    )
    raw_country = country_request.json()

    raw_holidays = {}
    used_dates = set()
    holiday_types = [
        'PUBLIC', 'RELIGIOUS', 'OBSERVANCE'
    ]
    for holiday_type in holiday_types:
        holiday_request_params = {
            'type': holiday_type,
            'country': iso,
            'sort': 'date-asc'
        }
        holiday_request = requests.get(
            url_for('get_holidays', _external=True),
            params=holiday_request_params
        )
        
        sub_holidays = holiday_request.json()
        raw_holidays[holiday_type] = sub_holidays
        for value in sub_holidays['holidays'].values():
            used_dates.add(value['date'])

    date_request = requests.get(
        url_for('get_dates', _external=True),
        params={
            'specify': ','.join(used_dates)
        }
    )
    raw_dates = date_request.json()['dates']

    country = {
        'name': raw_country[iso]['name'],
        'iso': iso,
        'region': raw_country[iso]['region'],
        'capital': raw_country[iso]['capital'],
        'population': format_population(
            raw_country[iso]['population']
        )
    }

    holiday_type_to_title = {
        'PUBLIC': 'Public Holidays',
        'RELIGIOUS': 'Religious Holidays',
        'OBSERVANCE': 'Observances'
    }
    sub_holidays = {
        holiday_type_to_title[holiday_type]: [
            [
                (
                    raw_holidays[holiday_type]['holidays'][str(id_)]['name'],
                    url_for('holidays', id=id_)
                ),
                (
                    raw_dates[raw_holidays[holiday_type]['holidays'][str(id_)]['date']]['formatted'],
                    url_for('dates_by_day', date_code=raw_holidays[holiday_type]['holidays'][str(id_)]['date'])
                )
            ] for id_ in raw_holidays[holiday_type]['ordered_keys']
        ] for holiday_type in holiday_types
    }

    return render_template(
        'countries/country.html', 
        country=country, 
        holidays=sub_holidays
    )



if __name__ == "__main__":
    app.run(debug=True, host='127.0.0.1', port=8080)

