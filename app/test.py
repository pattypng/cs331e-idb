import os
import sys
import unittest
from models import app, db, Country, Holiday, Date


class DBTestCases(unittest.TestCase):
    # ---------
    # insertion
    # ---------
    
    def test_country_1(self):
        country = Country(name = 'Genovia', capital = 'Costa Luna', population = '10.5M', region = 'Southern Europe', iso = 'GV')
        db.session.add(country)
        db.session.commit()


        r = db.session.query(Country).filter_by(iso='GV').one()
        self.assertEqual(str(r.iso), 'GV')

        db.session.query(Country).filter_by(iso='GV').delete()
        db.session.commit()
        
    def test_country_2(self):
        country = Country(name = 'Wakanda', capital = 'Birnin Zana', population = '6M', region = 'Middle Africa', iso = 'WK')
        db.session.add(country)
        db.session.commit()


        r = db.session.query(Country).filter_by(iso = 'WK').one()
        self.assertEqual(str(r.iso), 'WK')

        db.session.query(Country).filter_by(iso = 'WK').delete()
        db.session.commit()
        
    def test_country_3(self):
        country = Country(name = 'Panem', capital = 'Capitol', population = '4.5M', region = 'Northern America', iso = 'PN')
        db.session.add(country)
        db.session.commit()


        r = db.session.query(Country).filter_by(iso = 'PN').one()
        self.assertEqual(str(r.iso), 'PN')

        db.session.query(Country).filter_by(iso = 'PN').delete()
        db.session.commit()
    
    def test_holiday_1(self):
        holiday = Holiday(id = 80085, name = "Patty's Birthday", type = 'PUBLIC', url = 'pattysbirthday', description = 'a day of glabal celebration, everyone rejoice!', fixed = True, date='2024-01-01')
        db.session.add(holiday)
        db.session.commit()


        r = db.session.query(Holiday).filter_by(id = 80085).one()
        self.assertEqual(str(r.id), '80085')

        db.session.query(Holiday).filter_by(id = 80085).delete()
        db.session.commit()
        
    def test_holiday_2(self):
        holiday = Holiday(id = 80086, name = "Rohan Day", type = 'RELIGIOUS', url = "rohanday", fixed = True, date='2024-03-07')
        db.session.add(holiday)
        db.session.commit()


        r = db.session.query(Holiday).filter_by(id = 80086).one()
        self.assertEqual(str(r.id), '80086')

        db.session.query(Holiday).filter_by(id = 80086).delete()
        db.session.commit()
        
    def test_holiday_3(self):
        holiday = Holiday(id = 80087, name = "Nayan and Vardaan Day of Observance", type = 'OBSERVANCE', url = "nayanvardaan", fixed = False, date='2024-02-09')
        db.session.add(holiday)
        db.session.commit()


        r = db.session.query(Holiday).filter_by(id = 80087).one()
        self.assertEqual(str(r.id), '80087')

        db.session.query(Holiday).filter_by(id = 80087).delete()
        db.session.commit()

    def test_date_1(self):
        date = Date(date = '2025-01-01', url ='2025-01-01', day_of_week = 'Sunday', formatted = 'January 1, 2025')
        db.session.add(date)
        db.session.commit()


        r = db.session.query(Date).filter_by(date = '2025-01-01').one()
        self.assertEqual(str(r.date), '2025-01-01')

        db.session.query(Date).filter_by(date = '2025-01-01').delete()
        db.session.commit()
        
    def test_date_2(self):
        date = Date(date = '2025-03-14', url ='2025-03-14', day_of_week = 'Friday', formatted = 'March 13, 2025')
        db.session.add(date)
        db.session.commit()


        r = db.session.query(Date).filter_by(date = '2025-03-14').one()
        self.assertEqual(str(r.date), '2025-03-14')

        db.session.query(Date).filter_by(date = '2025-03-14').delete()
        db.session.commit()
        
    def test_date_3(self):
        date = Date(date = '2025-12-25', url ='2025-12-25', day_of_week = 'Wednesday', formatted = 'Decemeber 25, 2025')
        db.session.add(date)
        db.session.commit()


        r = db.session.query(Date).filter_by(date = '2025-12-25').one()
        self.assertEqual(str(r.date), '2025-12-25')

        db.session.query(Date).filter_by(date = '2025-12-25').delete()
        db.session.commit()



if __name__ == '__main__':
    with app.app_context():
        unittest.main()