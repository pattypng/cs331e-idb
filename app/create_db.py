import json
from datetime import datetime, timedelta
from pathlib import Path
from sqlalchemy import func

from models import app, db, Date, Holiday, Country


THIS_DIRECTORY = Path(__file__).parent
DATA_DIRECTORY = THIS_DIRECTORY / 'data'


def create_dates_db():
    start = datetime(2024, 1, 1)
    end = datetime(2024, 12, 31)
    dates = [
        start + timedelta(days=i) 
        for i in range((end - start).days + 1)
    ]
    for date in dates:
        date_obj = Date(
            date=date.date(),
            day_of_week=date.strftime('%A'),
            formatted=date.strftime('%B %d, %Y')
        )

        db.session.add(date_obj)


    db.session.commit()


def load_holidays():
    with open(DATA_DIRECTORY / 'holidays.json') as f:
        holiday_data = json.load(f)
    return holiday_data


def create_holidays_db():
    holiday_data = load_holidays()
    for i, holiday in enumerate(holiday_data):
        holiday_obj = Holiday(
            id=i,
            name=holiday['name'],
            type=holiday['type'],
            description=holiday['description'],
            fixed=holiday['fixed'],
            date=Date.query.get(
                datetime.strptime(holiday['date'], '%Y-%m-%d').date() 
            ).date
        )

        db.session.add(holiday_obj)
  
    db.session.commit()


def load_countries():
    with open(DATA_DIRECTORY / 'countries.json') as f:
        country_data = json.load(f)
    return country_data


def create_countries_db():
    country_data = load_countries()
    for country in country_data:        
        country_obj = Country(
            iso=country['country_code'],
            name=country['name'],
            capital=country['capital'],
            population=country['population'],
            region=country['region']
        )


        db.session.add(country_obj)

    db.session.commit()


def create_holiday_country(holiday_data, all_holidays, all_countries):
    for holiday in all_holidays:
        for country in all_countries:
            if country.iso in holiday_data[holiday.id]['ISOs']:
                holiday.countries.append(country)

    for country in all_countries:
        for holiday in all_holidays:
            if country.iso in holiday_data[holiday.id]['ISOs']:
                country.holidays.append(holiday)

    db.session.commit()


def create_everything():
    with app.app_context():
        db.drop_all()
        db.create_all()
        create_dates_db()
        create_countries_db()
        create_holidays_db()

        all_holidays = Holiday.query.all()
        all_countries = Country.query.all()
        holiday_data = load_holidays()

        create_holiday_country(holiday_data, all_holidays, all_countries)

        


if __name__ == "__main__":
    create_everything()
