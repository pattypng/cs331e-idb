import os
import json
from collections import defaultdict
from pathlib import Path
from titlecase import titlecase

THIS_DIRECTORY = Path(__file__).parent


def is_empty(file_path):
    return os.stat(file_path).st_size == 0


def aggregate_country_info() -> list[dict[str, str]]:
    folder_path = THIS_DIRECTORY / 'countries'

    all_data = []
    for file_name in os.listdir(folder_path):
        if file_name.endswith(".json"):
            file_path = folder_path / file_name
            if is_empty(file_path):
                continue

            with open(file_path) as f:
                data = json.load(f)
                country_code = file_name.split(".")[0]
                data['country_code']  = country_code
                data['population'] = int(data['population'])

                all_data.append(data)

    return all_data


def aggregate_api_ninjas() -> list[dict[str, str]]:
    folder_path = THIS_DIRECTORY / 'holidays/api-ninjas'

    all_data = []
    for file_name in os.listdir(folder_path):
        if file_name.endswith(".json"):
            file_path = folder_path / file_name
            if is_empty(file_path):
                continue

            with open(file_path) as f:
                raw_data = json.load(f)
                data = [{
                    'iso': raw_holiday['iso'].upper(),
                    'date': raw_holiday['date'],
                    'name': titlecase(raw_holiday['name']),
                    'type': raw_holiday['type']
                } for raw_holiday in raw_data]

                all_data.extend(data)

    return all_data


def aggregate_nager() -> list[dict[str, str | bool | list[str]]]:
    folder_path = THIS_DIRECTORY / 'holidays/nager'

    all_data = []
    for file_name in os.listdir(folder_path):
        if file_name.endswith(".json"):
            file_path = folder_path / file_name
            if is_empty(file_path):
                continue

            with open(file_path) as f:
                raw_data = json.load(f)
                data = [{
                    'iso': raw_holiday['countryCode'].upper(),
                    'date': raw_holiday['date'],
                    'name': titlecase(raw_holiday['name']),
                    'fixed': raw_holiday['fixed'],
                    'types': raw_holiday['types']
                } for raw_holiday in raw_data]

                all_data.extend(data)

    return all_data


def aggregate_calendarific() -> list[dict[str, str | bool | list[str]]]:
    folder_path = THIS_DIRECTORY / 'holidays/calendarific'

    all_data = []
    for file_name in os.listdir(folder_path):
        if file_name.endswith(".json"):
            file_path = folder_path / file_name
            if is_empty(file_path):
                continue

            with open(file_path) as f:
                raw_data = json.load(f)
                data = [{
                    'iso': raw_holiday['country']['id'].upper(),
                    'date': raw_holiday['date']['iso'],
                    'name': titlecase(raw_holiday['name']),
                    'type': raw_holiday['primary_type'],
                    'description': raw_holiday['description']
                } for raw_holiday in raw_data]

                all_data.extend(data)

    return all_data


def get_formatted_ungrouped_data():
    with open(THIS_DIRECTORY / 'api_ninjas.json') as f:
        api_ninjas_data = json.load(f)
    with open(THIS_DIRECTORY / 'nager.json') as f:
        nager_data = json.load(f)
    with open(THIS_DIRECTORY / 'calendarific.json') as f:
        calendarific_data = json.load(f)

    return api_ninjas_data, nager_data, calendarific_data


def group_all(
        api_ninjas_data,
        nager_data,
        calendarific_data
) -> dict[str, str | list[str]]:    
    name_date = {
        (holiday['name'], holiday['date']) 
        for holiday in api_ninjas_data
    }
    name_date = name_date.intersection({
        (holiday['name'], holiday['date']) 
        for holiday in nager_data
    })
    name_date = name_date.intersection({
        (holiday['name'], holiday['date']) 
        for holiday in calendarific_data
    })

    all_data = api_ninjas_data + nager_data + calendarific_data
    name_date_to_countries = defaultdict(set)
    for name, date in name_date:
        for holiday in all_data:
            if holiday['name'] == name and holiday['date'] == date:
                name_date_to_countries[(name, date)].add(holiday['iso'])

    new_data = []
    for (name, date), countries in name_date_to_countries.items():
        countries = list(countries)
        holiday = {
            'ISOs': countries,
            'date': date,
            'name': name
        }
        new_data.append(holiday)

    return new_data


def get_types(
        api_ninjas_data,
        calendarific_data
) -> list[str]:
    data = api_ninjas_data + calendarific_data
    
    types = set()
    for holiday in data:
        types.add(holiday['type'])
    
    return list(types)


def get_types_map():
    with open(THIS_DIRECTORY / 'types_map.json') as f:
        types_map = json.load(f)
    
    return types_map


def get_formatted_grouped_data():
    with open(THIS_DIRECTORY / 'grouped_holidays.json') as f:
        data = json.load(f)

    return data


def reconcile_types(
        grouped_data,
        api_ninjas_data,
        calendarific_data,
        types_map
):
    for holiday in grouped_data:
        types = defaultdict(int)
        name, date = holiday['name'], holiday['date']
        for source in [api_ninjas_data, calendarific_data]:
            for compare_holiday in source:
                if compare_holiday['name'] == name and compare_holiday['date'] == date:
                    mapped_type = types_map[compare_holiday['type']]
                    types[mapped_type] += 1
        best_type = max(types, key=types.get)
        holiday['type'] = best_type

    return grouped_data


def add_description(
        grouped_data,
        calendarific_data
):
    for holiday in grouped_data:
        descriptions = defaultdict(int)
        name, date = holiday['name'], holiday['date']
        for compare_holiday in calendarific_data:
            if compare_holiday['name'] == name and compare_holiday['date'] == date:
                descriptions[compare_holiday['description']] += 1

        best_description = max(descriptions, key=descriptions.get)
        holiday['description'] = best_description

    return grouped_data


def add_fixed(
        grouped_data,
        nager_data
):
    for holiday in grouped_data:
        name, date = holiday['name'], holiday['date']
        for compare_holiday in nager_data:
            if compare_holiday['name'] == name and compare_holiday['date'] == date:
                fixed = compare_holiday['fixed']
                break

        holiday['fixed'] = fixed

    return grouped_data


def main() -> None:
    all_data = aggregate_country_info()
    with open(THIS_DIRECTORY / 'countries.json', 'w') as f:
        json.dump(all_data, f, indent=4, ensure_ascii=False)

    # api_ninjas_data, nager_data, calendarific_data = get_formatted_ungrouped_data()
    # formatted_grouped_data = group_all(
    #     api_ninjas_data,
    #     nager_data,
    #     calendarific_data
    # )

    # types_map = get_types_map()
    # type_reconciled_data = reconcile_types(
    #     formatted_grouped_data,
    #     api_ninjas_data,
    #     calendarific_data,
    #     types_map
    # )

    # description_added_data = add_description(
    #     type_reconciled_data,
    #     calendarific_data
    # )

    # fixed_added_data = add_fixed(
    #     description_added_data,
    #     nager_data
    # )

    # unique_name_counts = defaultdict(int)
    # for holiday in fixed_added_data:
    #     unique_name_counts[holiday['name']] += 1
    
    # sorted_unique_name_counts = sorted(unique_name_counts.items(), key=lambda x: x[1], reverse=True)
    # for name, count in sorted_unique_name_counts:
    #     print(name, count)



    # with open(THIS_DIRECTORY / 'holidays.json', 'w') as f:
    #     json.dump(fixed_added_data, f, indent=4, ensure_ascii=False)
    


if __name__ == "__main__":
    main()
    