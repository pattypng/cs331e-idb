import json
import requests
from pathlib import Path

THIS_DIRECTORY = Path(__file__).parent

API_NINJAS_HOLIDAYS_ENDPOINT = 'https://api.api-ninjas.com/v1/holidays'
API_NINJAS_COUNTRIES_ENDPOINT = 'https://api.api-ninjas.com/v1/country'
API_NINJAS_HEADERS = {'X-Api-Key': 'aSHY4AJxm91pncMa19OI7Q==0RKVLp7J8Mpoxhcr'}
NAGER_ENDPOINT = 'https://date.nager.at/api/v3/publicholidays'
CALENDARIFIC_ENDPOINT = 'https://calendarific.com/api/v2/holidays'
CALENDARIFIC_API_KEY = 'YdRAMACwgS1w75L6QvvH8gxzwxlmPMih'
# ABSTRACT_API_ENDPOINT = 'https://holidays.abstractapi.com/v1/'
# ABSTRACT_API_KEY = '50ebc02e9f6344bdba06ba08b20b50ba'


def get_iso_codes(filepath: Path) -> dict[str, str]:
    with open(filepath) as f:
        return json.load(f)
ISO_CODES = get_iso_codes(THIS_DIRECTORY / 'iso codes.json')


def get_holidays_api_ninjas(country: str) -> str:
    params = {
        'country': country,
        'year': '2024',
    }
    response = requests.get(
        API_NINJAS_HOLIDAYS_ENDPOINT, 
        headers=API_NINJAS_HEADERS, 
        params=params
    )

    data: list = response.json()
    if not data:
        return ''
    data.sort(key=lambda x: x['date'])
    return json.dumps(data, indent=4)


def get_country_info_api_ninjas(country: str) -> str:
    params = {
        'name': country,
        'limit': 1
    }

    response = requests.get(
        API_NINJAS_COUNTRIES_ENDPOINT, 
        headers=API_NINJAS_HEADERS, 
        params=params)

    all_data: list = response.json()
    if not all_data:
        return ''
    our_data = {
        'name': all_data[0]['name'],
        'capital': all_data[0]['capital'],
        'population': all_data[0]['population'],
        'region': all_data[0]['region']
    }
    return json.dumps(our_data, indent=4)


def get_all_api_ninjas_data() -> None:
    for key in ISO_CODES.keys():
        holidays = get_holidays_api_ninjas(key)
        countries = get_country_info_api_ninjas(key)
        with open(THIS_DIRECTORY / f'holidays/api-ninjas/{key}.json', 'w') as f:
            f.write(holidays)
        with open(THIS_DIRECTORY / f'countries/{key}.json', 'w') as f:
            f.write(countries)


def get_nager(country: str) -> str:
    response = requests.get(
        f'{NAGER_ENDPOINT}/2024/{country}'
    )
    if response.status_code != 200:
        return ''
    data: list = response.json()
    data.sort(key=lambda x: x['date'])
    return json.dumps(data, indent=4)


def get_all_nager() -> None:
    for key in ISO_CODES.keys():
        holidays = get_nager(key)
        with open(THIS_DIRECTORY / f'holidays/nager/{key}.json', 'w') as f:
            f.write(holidays)


def get_calendarific(country: str) -> str:
    params = {
        'api_key': CALENDARIFIC_API_KEY,
        'country': country,
        'year': 2024
    }
    response = requests.get(
        CALENDARIFIC_ENDPOINT,
        params=params
    )
    response_json = response.json()['response']
    if not response_json:
        return ''
    data: list = response_json['holidays']
    data.sort(key=lambda x: x['date']['iso'])
    return json.dumps(data, indent=4)


def get_all_calendarific() -> None:
    for key in ISO_CODES.keys():
        holidays = get_calendarific(key)
        with open(THIS_DIRECTORY / f'holidays/calendarific/{key}.json', 'w') as f:
            f.write(holidays)

'''
def get_abstract(country: str) -> None:
    params = {
        'api_key': ABSTRACT_API_KEY,
        'country': country,
        'year': 2024
    }
    response = requests.get(
        ABSTRACT_API_ENDPOINT,
        params=params
    )
    data: list = response.json()
    print(data)
    if not data:
        return ''
    data.sort(key=lambda x: (x['date_year'], x['date_month'], x['date_day']))
    return json.dumps(data, indent=4)


def get_all_abstract() -> None:
    for key in ISO_CODES.keys():
        holidays = get_abstract(key)
        with open(THIS_DIRECTORY / f'holidays/abstract-api/{key}.json', 'w') as f:
            f.write(holidays)
'''

def main():
    ...


if __name__ == "__main__":
    main()
