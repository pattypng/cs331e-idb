ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYDOC    := pydoc3
    DOC := docker run -it -v $$(PWD):/usr/cs331e -w /usr/cs331e fareszf/python
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    DOC := docker run -it -v /$$(PWD):/usr/cs331e -w //usr/cs331e fareszf/python
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYDOC    := pydoc3
    DOC := docker run -it -v $$(PWD):/usr/cs331e -w /usr/cs331e fareszf/python
endif

versions:
	which       git
	git         --version
	@echo
	which       make
	make        --version
	@echo
	which       $(PIP)
	$(PIP)      --version
	@echo
	which        $(PYTHON)
	$(PYTHON)    --version

models.html: app/models.py
	$(PYDOC) -w app/models.py
	
IDB1.log:
	git log > IDB1.log

IDB2.log:
	git log > IDB2.log

IDB3.log:
	git log > IDB3.log

test:
	$(PYTHON) app/test.py
